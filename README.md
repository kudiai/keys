Keys
=============

This repository holds SSH keys for various uses by within Kudi, for example, allowing build systems (Semaphore CI) to access repositories, etc.
The keys are individually encrypted using my (Dorian) GPG key.

The main reason for keeping the keys in a repository is for backup purposes. We have already run into an incident where some important keys were kept on my (Dorian) laptop and all the keys were lost with a dead SSD. Unforunately this means all the keys currently in use will need to be reconfigured and also backed up -- thus, this repository forfills the 'backup' part of the recovery process.

Those that may need to configure services can be added as additional recipients on the encrypted keys, but as of right now, I am the only one to be configuring the services.

## Semantics and Directory Structure
The keys in this repository describe an principal and its identity, i.e. the kudi-core-service build process in semaphore is a principal, and is represented by the directory structure `semaphore/kudi-core-service`. The key pair contained within that folder are the private and public keys for representing its identity. These keys can then be used to enable access to specific entities and services. This is analogous to a group of people being assigned a key-card each, and each door is configured to open only if presented with a registered key-card.

**NOTE: Additional GPG users can only be appended (since we're storing these keys in git, any historical version can be retrieved meaning GPG users cannot be removed).**
